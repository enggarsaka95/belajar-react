import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyCY9iyUucPuP1CmxkkD-F-b0kFseCoWyB4',
  authDomain: 'm-care-ec4b7.firebaseapp.com',
  databaseURL: 'https://m-care-ec4b7-default-rtdb.firebaseio.com/',
  projectId: 'm-care-ec4b7',
  storageBucket: 'm-care-ec4b7.appspot.com',
  messagingSenderId: '531840567288',
  appId: '1:531840567288:web:9330ea0523ed7c027ff80d',
});

const Fire = firebase;

export default Fire;
